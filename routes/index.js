exports.index = function(req, res){
	res.render('calcwebservicemain', { title: 'Calculator Webservice !' });
};

exports.evaluate =  function(req, res){
	var obj = JSON.parse(JSON.stringify(req.body));
	var expression = obj.expression;
	var options = {
			//ignoredNamespaces: true
	}
	var soap = require('soap');
	var url = 'http://localhost:8080/CalculatorWebServiceJAXWSServer/webservices/Calculator?wsdl';
	var args = {arg0: expression};
	soap.createClient(url,options, function(err, client) {
		console.log(client.describe());
		client.CalculatorService.CalculatorServicePort.evaluate(args,function(err, result) {
			console.log(err);
			console.log(result);
			if(err){
				res.status(404).send("Error in Calling the SOAP WebService!!");
			}else{
				res.status(200).send(result.return);
			}
			//console.log(client.lastRequest);	
		});
	});
}